import json
from perform import perform

def consolidate(event, context):
    perform()

    body = {
        "message": "Consolidated...",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response