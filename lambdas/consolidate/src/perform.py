
import pandas as pd
import joblib
import numpy as np

from datetime import timedelta, datetime
from s3_local_client import store_on_s3, load_from_s3
from sklearn.preprocessing import Binarizer
from tweet_methods import aggregate_sentiment

def perform():
    yesterday = (datetime.utcnow() - timedelta(days=1)).strftime("%Y-%-m-%-d")

    # Load scalers
    scaler = joblib.load("./scaler.pkl")
    pca = joblib.load("./pca.pkl")

    # Load dfs
    eth_tweets = load_from_s3(f'eth-tweets/{yesterday}.csv').dropna()
    market_tweets = load_from_s3(f'market-tweets/{yesterday}.csv').dropna()
    eth_values = load_from_s3(f'eth-values/{yesterday}.csv')
    market_values = load_from_s3(f'market-values/{yesterday}.csv')
    raw_consolidated = load_from_s3('consolidated/raw_consolidated.csv')

    # Add weights to tweets dfs
    eth_tweets['weight'] = eth_tweets['likes'] + eth_tweets['replies'] + 1
    market_tweets['weight'] = market_tweets['likes'] + market_tweets['replies'] + 1

    # Consolidate sentiment
    eth_sentiment = pd.DataFrame(aggregate_sentiment(eth_tweets)).transpose().rename(columns={"compound": "eth_compound"})
    market_sentiment = pd.DataFrame(aggregate_sentiment(market_tweets)).transpose().rename(columns={"compound": "market_compound"})
    sentiment = eth_sentiment.merge(market_sentiment, left_index=True, right_index=True)

    # Create dataframe row
    df = eth_values.merge(market_values, left_index=True, right_index=True).merge(sentiment, left_index=True, right_index=True)
    df.index = pd.to_datetime([yesterday])
    df.rename(columns={"market_cap_x": "eth_market_cap", "market_cap_y": "market_cap"}, inplace=True)
    df = pd.concat([raw_consolidated, df])
    df.index = pd.to_datetime(df.index)

    # Store new dataframe
    store_on_s3('consolidated/raw_consolidated.csv', df)

    # Features
    # # Let's use the percentage change on all of this features instead of the current values
    sentiment_cols = ['eth_compound', 'market_compound']
    df[[f"{x}_log_ret" for x in df.columns.difference(sentiment_cols)]] = np.log(1 + df[df.columns.difference(sentiment_cols)].pct_change())

    df = df.iloc[1:]
    df['close_trend'] = Binarizer().fit_transform(df[['close_log_ret']])
    # Remove price columns
    price_cols = ['open', 'close', 'high', 'low', '24_volume', '24_volume_without_btc', 'eth_market_cap', 'volume', 'market_cap_without_btc', 'market_cap']
    df = df[df.columns.difference(price_cols)]

    # Target and features store
    target = df[['close_trend']]
    features = pd.DataFrame(pca.transform(scaler.transform(df)))
    features.index = target.index
    
    store_on_s3('consolidated/target.csv', target)
    store_on_s3('consolidated/features.csv', features)
