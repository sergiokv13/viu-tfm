import re
import pandas as pd

from contractions import contractions
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

analyzer = SentimentIntensityAnalyzer()

def preprocess_tweet(text):
    # Remove url
    text = re.sub(r'http\S+', '', text)
    # Contractions transformation
    text = " ".join([contractions[word] if word in contractions else word for word in text.split(" ")])
    # Remove hashtags
    text = text.replace("#", "")
    # Remove usernames
    text = re.sub(r'@+', '', text)
    # Lemmantization and Stemming
    text = " ".join(word for word in text.split(" "))
    
    return text

def aggregate_sentiment(group):
    group = group.reset_index()
    for idx,row in group.iterrows():
        tweet = preprocess_tweet(row['text'])
        vs = analyzer.polarity_scores(tweet)

        group.at[idx, 'compound'] = vs['compound']
        group.at[idx, 'pos'] = vs['pos']
        group.at[idx, 'neu'] = vs['neu']
        group.at[idx, 'neg'] = vs['neg']
    
    compound = (group['weight'] * group['compound']).sum() / group['weight'].sum()
    
    return pd.Series({"compound": compound})

