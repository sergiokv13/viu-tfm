import json
import re
from s3_local_client import load_from_s3

def hello(event, context):
    print("event: ", event, type(event))
    # location = re.search("'location': '(.*?)'}", event).group(1)

    print("QueryString ", event['rawQueryString'])
    location = event['rawQueryString'].replace("location=", "")

    print("location: ", location)

    df = load_from_s3(location).reset_index()

    if 'index' in df.columns:
        df.rename(columns={'index': 'date'}, inplace=True)

    body = {
        "message": "Success",
        "input": df.to_dict(orient="records")
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response

