from datetime import timedelta, datetime
import requests
import pandas as pd
import os
from s3_local_client import store_on_s3

baerer = "******************"

def create_headers():
    headers = {"Authorization": "Bearer {}".format(baerer)}
    return headers

def load_recent_tweets(
    words,
    element,
    iterations=7,
    lang="en",
):
    words_query = ('%20OR%20').join(words)

    start_date = (datetime.utcnow() - timedelta(days=1)).strftime("%Y-%m-%d") + "T00:00:00Z"
    end_date = datetime.utcnow().strftime("%Y-%m-%d") + "T00:00:00Z"

    next_token = None
    data = {"user": [], "text": [], "likes": [], "retweets": [], "replies": []}

    for i in range(iterations):
        endpoint = "https://api.twitter.com/2/tweets/search/recent"
        query = (
                f"query=({words_query})%20lang%3A{lang} -nft -is:retweet&"
                f"tweet.fields=public_metrics,text,author_id&"
                f"max_results=100&"
                f"start_time={start_date}&"
                f"end_time={end_date}&"
            )

        if (next_token):
            query = f"{query}&next_token={next_token}"


        res = requests.get(f'{endpoint}?{query}', headers=create_headers())
        next_token = res.json()['meta']['next_token']

        response_data = res.json()['data']

        for tweet in response_data:
            data["user"].append(tweet['author_id'])
            data["text"].append(tweet['text'])
            data["likes"].append(tweet['public_metrics']['like_count'])
            data["retweets"].append(tweet['public_metrics']['quote_count'])
            data["replies"].append(tweet['public_metrics']['reply_count'])
    
    df = pd.DataFrame(data).dropna()
    yesterday = (datetime.utcnow() - timedelta(days=1)).strftime("%Y-%-m-%-d")
    store_on_s3(f'{element}-tweets/{yesterday}.csv', df)
