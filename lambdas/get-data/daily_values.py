from datetime import timedelta, datetime
import requests
import pandas as pd
from s3_local_client import store_on_s3

def load_and_store_market_values():
    # get the data for today 00:00
    start_date = (datetime.utcnow() - timedelta(days=1)).strftime("%Y-%-m-%-d")
    end_date = datetime.utcnow().strftime("%Y-%-m-%-d")

                              
    res = requests.get(f'https://web-api.coinmarketcap.com/v1.1/global-metrics/quotes/historical?format=chart&interval=1d&time_end={end_date}&time_start={start_date}')
    response_data = res.json()['data']
    response_df = (
        pd.DataFrame(list(response_data.values())[0])
        .transpose()
        .rename(columns={0: 'market_cap', 1: '24_volume'})
        
    )

    res = requests.get(f'https://web-api.coinmarketcap.com/v1.1/global-metrics/quotes/historical?format=chart_altcoin&interval=1d&time_end={end_date}&time_start={start_date}')
    response_data = res.json()['data']
    response_df_2 = (
        pd.DataFrame(list(response_data.values())[0])
        .transpose()
        .rename(columns={0: 'market_cap_without_btc', 1: '24_volume_without_btc'})
        
    )

    response_df = response_df.merge(response_df_2, left_index=True, right_index=True)
    store_on_s3(f'market-values/{start_date}.csv', response_df)

def load_and_store_eth_values():
    start_date = (datetime.utcnow() - timedelta(days=2)).strftime("%Y-%-m-%-d")
    end_date = (datetime.utcnow() - timedelta(days=1)).strftime("%Y-%-m-%-d")

                              
    res = requests.get(f'https://web-api.coinmarketcap.com/v1/cryptocurrency/ohlcv/historical?id=1027&convert=USD&time_start={start_date}&time_end={end_date}')    
    response_data = res.json()['data']['quotes'][0]['quote']['USD']
    response_df = pd.DataFrame([response_data]).drop(columns=['timestamp'])

    store_on_s3(f'eth-values/{end_date}.csv', response_df)