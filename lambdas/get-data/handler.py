import json
from daily_values import load_and_store_eth_values, load_and_store_market_values
from daily_tweets import load_recent_tweets

def hello(event, context):

    # Load and store market values
    load_and_store_eth_values()
    print("ETH values stored")
    load_and_store_market_values()
    print("Market values stored")

    eth = ['%23eth', '%23ethereum', '%23ether', '%23ethercoin']
    market = ['%23crypto', '%23defi', '%23cryptocurrency']

    # Load tweets
    load_recent_tweets(eth, 'eth')
    print("ETH tweets stored")
    load_recent_tweets(market, 'market')
    print("Market tweets stored")

    body = {
        "message": "Getting the data...",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
