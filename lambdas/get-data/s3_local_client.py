import io
import boto3

s3_client = boto3.client(
    "s3",
    aws_access_key_id="******************",
    aws_secret_access_key="******************",
)

def store_on_s3(location, df):
    with io.StringIO() as csv_buffer:
        df.to_csv(csv_buffer)

        response = s3_client.put_object(
            Bucket="forecasting-eth-data", Key=location, Body=csv_buffer.getvalue()
        )

        status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")

        if status == 200:
            print(f"Successful S3 put_object response on {location}. Status - {status}")
        else:
            raise Error("Unable to put the tweets to s3")