import json
 
from train_fit import train

def train_and_forecast(event, context):
    train()

    body = {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "input": event
    }

    response = {
        "statusCode": 200,
        "body": "Success"
    }

    return response