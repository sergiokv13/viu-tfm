from s3_local_client import load_from_s3, store_on_s3
import pandas as pd
import tensorflow as tf
import numpy as np
from datetime import timedelta, datetime

Adam = tf.keras.optimizers.Adam


target = load_from_s3("consolidated/target.csv")
features = load_from_s3("consolidated/features.csv")

def look_up(observations, x_data, y_data):
    new_x_data = []
    new_y_data = []

    for i in range(observations ,len(x_data)):
      new_x_data.append(x_data[i-observations : i, : x_data.shape[1]])
      new_y_data.append(y_data[i]) # predict the next record

    new_x_data = np.array(new_x_data)
    new_y_data = np.array(new_y_data)

    new_x_data = np.reshape(new_x_data, (new_x_data.shape[0], new_x_data.shape[1], new_x_data.shape[2]))
    new_y_data = new_y_data[:,0]

    return new_x_data, new_y_data

def train_fit_model(model_name, n_steps, model_params):
    predicted = load_from_s3(f"predictions/predicted_{model_name}_model.csv")

    # Model and train
    model = tf.keras.models.load_model(f'./models/{model_name}.h5')
    optimizer=Adam(model_params["learning_rate"])
    model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'] )

    x_train, y_train = look_up(n_steps, features.values, target.values)

    if 'batch_size' in model_params:
        model.fit(x_train, y_train, epochs=model_params['epochs'], batch_size=model_params['batch_size'])
    else:
        model.fit(x_train, y_train, epochs=model_params['epochs'])
            
    # Predict and create new df
    observation = np.array([features.values[n_steps * -1:]])
    today = datetime.utcnow().strftime("%Y-%m-%d")

    daily_prediction = model.predict(observation)[0,0]
    pred_df = pd.DataFrame([{'date': today, 'predicted': daily_prediction}],).set_index('date')
    new_predictions = pd.concat([predicted, pred_df])
    new_predictions.index = pd.to_datetime(new_predictions.index)

    # Check if we have all the dates
    index_list = new_predictions.index

    diff = list(pd.date_range(start=index_list[0], end=index_list[-1]).difference(new_predictions.index))

    if diff:
        raise  ValueError(f"There are missing dates on the predictions: {diff}")

    store_on_s3(f"predictions/predicted_${model_name}_model.csv", new_predictions)

    return daily_prediction

def train_fit_combined(f1, f2, f3):
    predicted_soft = load_from_s3("predictions/predicted_combined_soft.csv")
    predicted_hard = load_from_s3("predictions/predicted_combined_hard.csv")
    today = datetime.utcnow().strftime("%Y-%m-%d")

    # 1 or 0 instead of prob
    get_vote = lambda x: 1 if x > 0.5 else 0
    soft = (f1 + f2 + f3) / 3
    hard = 1 if get_vote(1) + get_vote(2) + get_vote(3) >= 2 else 0

    soft_df = pd.DataFrame([{'date': today, 'predicted': soft}],).set_index('date')
    new_predictions = pd.concat([predicted_soft, soft_df])
    new_predictions.index = pd.to_datetime(new_predictions.index)

    store_on_s3("predictions/predicted_combined_soft.csv", new_predictions)

    hard_df = pd.DataFrame([{'date': today, 'predicted': hard}],).set_index('date')
    new_predictions = pd.concat([predicted_hard, hard_df])
    new_predictions.index = pd.to_datetime(new_predictions.index)

    store_on_s3("predictions/predicted_combined_hard.csv", new_predictions)

def train():
    f1 = train_fit_model("first", 7, model_params={"epochs": 38, "learning_rate": 0.01, "batch_size": 16})
    f2 = train_fit_model("second", 60, model_params={"epochs": 38, "learning_rate": 0.01, "batch_size": 16})
    f3 = train_fit_model("third", 60, model_params={"epochs": 38, "learning_rate": 0.01, "batch_size": 16})

    train_fit_combined(f1, f2, f3)
    