import React from 'react';

import {
  Box,
  Typography,
  CircularProgress,
} from '@material-ui/core';

import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  loadingCopy: {
    color: '#e8560f',
    fontSize: '13px',
  },
}));

const CenteredLoader = ({ top, label, height }) => {
  const classes = useStyles();

  return (
    <Box
      display="flex"
      marginTop={top}
      flexDirection="column"
      flex={1}
      alignItems="center"
      justifyContent="center"
      style={{ height: height }}
    >
      <Box mb={2}>
        <Typography className={classes.loadingCopy}>
          {label}
        </Typography>
      </Box>

      <CircularProgress size={50} style={{ color: '#e8560f' }}/>
    </Box>
  );
};

CenteredLoader.defaultProps = {
  top: '0px',
  height: '100%',
};

CenteredLoader.propTypes = {
  top: PropTypes.string,
  label: PropTypes.string,
  height: PropTypes.string,
};

export default CenteredLoader;
