import React from 'react'
import { isNil } from 'lodash';
import { DashboardContext } from '../dashboard-context';
import Wrapper from './wrapper';
import { getAccuracy } from '../../transformations/get-accuracy';

const Accuracy = ({ model, predicted, title="Accuracy", onlyOne = false }) => {
  const context = React.useContext(DashboardContext);
  const { target, am4charts, am4core } = context;

  React.useEffect(() => {
    !isNil(target) && !isNil(predicted) && am4core.ready(() => {
        // Create chart instance
        var chart = am4core.create(`accuracy-chart-${model}`, am4charts.XYChart);
        chart.numberFormatter.numberFormat = '#.##%';

        // Add data
        chart.data = getAccuracy(target, predicted, onlyOne);
        
        // Create axes
        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "range";
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.minGridDistance = 30;
        categoryAxis.tooltip.disabled = true;
        categoryAxis.renderer.minHeight = 110;
        
        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.minWidth = 50;
        valueAxis.min = 0;
        valueAxis.max = 1; 
        
        // Create series
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.sequencedInterpolation = true;
        series.dataFields.valueY = "value";
        series.dataFields.categoryX = "range";
        series.tooltipText = "[{categoryX}: bold]{valueY}[/]";
        series.columns.template.strokeWidth = 0;
        
        series.tooltip.pointerOrientation = "vertical";
        
        series.columns.template.column.cornerRadiusTopLeft = 10;
        series.columns.template.column.cornerRadiusTopRight = 10;
        series.columns.template.column.fillOpacity = 0.8;
        
        // on hover, make corner radiuses bigger
        var hoverState = series.columns.template.column.states.create("hover");
        hoverState.properties.cornerRadiusTopLeft = 0;
        hoverState.properties.cornerRadiusTopRight = 0;
        hoverState.properties.fillOpacity = 1;
        
        series.columns.template.adapter.add("fill", function(fill, target) {
          return chart.colors.getIndex(target.dataItem.index);
        });
        
        // Cursor
        chart.cursor = new am4charts.XYCursor();

        chart.logo.disabled = true;
    }); 
  }, [target, predicted]);

  return (
    <Wrapper title={title}>
      <div id={`accuracy-chart-${model}`} style={{width: '100%', height: '40vh'}}>
      </div>
    </Wrapper>
  )
};

export default Accuracy;
