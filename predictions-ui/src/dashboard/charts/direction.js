import React from 'react'
import { isNil } from 'lodash';
import { DashboardContext } from '../dashboard-context';
import Wrapper from './wrapper';

const Direction = ({ model, predicted }) => {
  const context = React.useContext(DashboardContext);
  const { consolidated, am4charts, am4core } = context;

  const prepareData = () => {
    const boxData = [];
    for (let i = 1; i < consolidated.length; i++) {
      if (consolidated[i]['date'].includes('2021') || consolidated[i]['date'].includes('2022')) {
        const open = consolidated[i-1]['close'];
        const close = consolidated[i]['close'];
        const trend = close - open > 0.5 ? "UP" : "DOWN"
        const pred = predicted.find((pred) => pred.date === consolidated[i]['date'])?.predicted;

        boxData.push({
          date: consolidated[i]['date'],
          close: consolidated[i]['close'],
          open: consolidated[i-1]['close'],
          trend,
          predicted: !isNil(pred) ? `${(pred * 100).toFixed(2)}%` : "NP",
          predicted_trend: !isNil(pred) ? `${pred > 0.5 ? "UP" : "DOWN"}` : "NP",
      })
      }
    }
    return boxData;
  }

  const createLabel = (field, title, info) => {
    var titleLabel = info.createChild(am4core.Label);
    titleLabel.text = title + ":";
    titleLabel.marginRight = 5;
    titleLabel.minWidth = 60;

    var valueLabel = info.createChild(am4core.Label);
    valueLabel.id = field;
    valueLabel.text = "-";
    valueLabel.minWidth = 50;
    valueLabel.marginRight = 30;
    valueLabel.fontWeight = "bolder";
  }

  const updateValues = (dataItem, chart) => {
    am4core.array.each(["date", "open", "close", "trend", "predicted", "predicted_trend"], function(key) {
      var series = chart.series.getIndex(0);
      var label = chart.map?.getKey(key);
      if (label)
      {
        if (['open', 'close'].includes(key)) label.text = chart.numberFormatter.format(dataItem[key + "ValueY"]);
        else label.text = dataItem[key + "ValueY"];

        if (dataItem[key + "ValueY"] === 'UP') label.fill = am4core.color("rgb(126, 204, 121)");
        else if (dataItem[key + "ValueY"] === 'DOWN') label.fill = am4core.color("rgb(205, 110, 110)");
        else label.fill = am4core.color("black");
      }
    });
  }


  React.useEffect(() => {
    consolidated.length > 0 && am4core.ready(function() {
      var chart = am4core.create(`direction-chart-${model}`, am4charts.XYChart);
      chart.paddingRight = 20;
      
      chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";
      
      var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
      dateAxis.renderer.grid.template.location = 0;
      
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis.tooltip.disabled = true;
      
      var series = chart.series.push(new am4charts.CandlestickSeries());
      series.dataFields.dateX = "date";
      series.dataFields.valueY = "close";
      series.dataFields.openValueY = "open";
      series.dataFields.lowValueY = "open";
      series.dataFields.highValueY = "close";
      series.dataFields.highValueY = "close";
      series.dataFields.trendValueY = "trend";
      series.dataFields.dateValueY = "date";
      series.dataFields.predictedValueY = "predicted";
      series.dataFields.predicted_trendValueY = "predicted_trend";

      series.simplifiedProcessing = true;
      
      chart.cursor = new am4charts.XYCursor();
      
      // a separate series for scrollbar
      var lineSeries = chart.series.push(new am4charts.LineSeries());
      lineSeries.dataFields.dateX = "date";
      lineSeries.dataFields.valueY = "close";
      // need to set on default state, as initially series is "show"
      lineSeries.defaultState.properties.visible = false;
      
      // hide from legend too (in case there is one)
      lineSeries.hiddenInLegend = true;
      lineSeries.fillOpacity = 0.5;
      lineSeries.strokeOpacity = 0.5;
      
      var scrollbarX = new am4charts.XYChartScrollbar();
      scrollbarX.series.push(lineSeries);
      chart.scrollbarX = scrollbarX;
      chart.scrollbarX.parent = chart.bottomAxesContainer;
      
      chart.data = prepareData();

      // Tooltip
      var info = chart.plotContainer.createChild(am4core.Container);
      info.width = 320;
      info.height = 140;
      info.x = 10;
      info.y = 10;
      info.padding(10, 10, 10, 10);
      info.background.fill = am4core.color("#000");
      info.background.fillOpacity = 0.1;
      info.layout = "grid";
      info.zIndex = 1000;
      
      createLabel("date", "OPEN", info);
      createLabel("open", "OPEN", info);
      createLabel("close", "CLOSE", info);
      createLabel("trend", "TREND", info);
      createLabel("predicted_trend", "PREDICTED TREND", info);
      createLabel("predicted", "PREDICTED %", info);

      chart.events.on("ready", function(ev) {
        updateValues(series.dataItems.last, chart);
      });

      chart.cursor.events.on("cursorpositionchanged", function(ev) {
        let dataItem = dateAxis.getSeriesDataItem(
          series,
          dateAxis.toAxisPosition(chart.cursor.xPosition),
          true
        );
        updateValues(dataItem, chart);
      });

      chart.logo.disabled = true;
    });
  }, [consolidated]);

  return (
    <Wrapper title="Movimientos de precio y predicciones pasadas">
      <div id={`direction-chart-${model}`} style={{width: '100%', height: '40vh'}}>
      </div>
    </Wrapper>
  )
};

export default Direction;
