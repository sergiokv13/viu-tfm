import React from 'react'
import { isNil } from 'lodash';
import { DashboardContext } from '../dashboard-context';
import Wrapper from './wrapper';

const Prediction = ({ model, predicted, title="Ultima predicción" , onlyOne=false}) => {
  const context = React.useContext(DashboardContext);
  const { am4charts, am4core } = context;

  const prediction = { 
    predicted: predicted[predicted.length - 1].predicted * 100, 
    date:  predicted[predicted.length - 1].date 
  };

  React.useEffect(() => {
    !isNil(predicted) && am4core.ready(() => {        
        // create chart
        var chart = am4core.create(`prediction-chart-${model}`, am4charts.GaugeChart);
        chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
        
        chart.innerRadius = -25;
        
        var axis = chart.xAxes.push(new am4charts.ValueAxis());
        axis.min = 0;
        axis.max = 100;
        axis.strictMinMax = true;
        axis.renderer.grid.template.stroke = new am4core.InterfaceColorSet().getFor("background");
        axis.renderer.grid.template.strokeOpacity = 0.3;
              
        var range0 = axis.axisRanges.create();
        range0.value = 0;
        range0.endValue = 50;
        range0.axisFill.fillOpacity = 1;
        range0.axisFill.fill = am4core.color("rgb(205, 110, 110)");
        range0.axisFill.zIndex = - 1;
        
        var range1 = axis.axisRanges.create();
        range1.value = 50;
        range1.endValue = 100;
        range1.axisFill.fillOpacity = 1;
        range1.axisFill.fill = am4core.color("rgb(126, 204, 121)");
        range1.axisFill.zIndex = -1;

        var label = chart.radarContainer.createChild(am4core.Label);
        label.isMeasured = false;
        label.fontSize = 45;
        label.horizontalCenter = "middle";
        label.verticalCenter = "bottom";
        const bgcolor = prediction.predicted > 50 ? am4core.color("rgb(126, 204, 121)") : am4core.color("rgb(205, 110, 110)");
        if (onlyOne){
          label.html = `
          <div style="background-color: ${bgcolor}; color: white; border-radius: 8px; padding: 8px; text-aling: 'center'; font-size: 30px;">
              Fecha: ${prediction.date} GMT<br/>
          </div>
          `;
        }
        else {
          label.html = `
          <div style="background-color: ${bgcolor}; color: white; border-radius: 8px; padding: 8px; text-aling: 'center'; font-size: 30px;">
              Fecha: ${prediction.date} GMT<br/>
              Tendencia: ${prediction.predicted.toFixed(2)}%
          </div>
          `;
        }

        
        let hand = chart.hands.push(new am4charts.ClockHand());
        hand.value = prediction.predicted;

        chart.logo.disabled = true;

        chart.preloader.disabled = false;

    });
  }, [predicted]);

  return (
    <Wrapper title={title}>
      <div id={`prediction-chart-${model}`} style={{width: '100%', height: '40vh'}}>
      </div>
    </Wrapper>
  )
};

export default Prediction;
