import React from 'react'
import { DashboardContext } from '../dashboard-context';

import { Box, Card, Typography  } from '@material-ui/core';
import { makeStyles  } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  widgetHeader: {
    color: 'white',
    background: '#e8560f',
    padding: '10px 10px',
    display: 'flex',
    alignItems: 'center',
  },
  editContainer: {
    display: 'flex',
    minWidth: 0,
    alignItems: 'center',
    padding: '0.25rem 0.5rem',
    border: '1px solid transparent',
    borderRadius: '4px',
    '& h5': {
      fontSize: '1rem',
    },
    '& .MuiButtonBase-root': {
      visibility: 'hidden',
      minWidth: 26,
    },
    '&:hover': {
      '& .MuiButtonBase-root': {
        visibility: 'visible',
      },
    }
  },
}));

const Direction = ({ children, title }) => {
  const context = React.useContext(DashboardContext);

  const classes = useStyles();
  return (
      <Card>
        <Box className={classes.widgetHeader}>
          <Box style={{ flex: 1 }}>
            <Typography variant="body2">
              <Box
                className={classes.editContainer}
              >
                <strong>{title}</strong>
              </Box>
            </Typography>
          </Box>
        </Box>

        <Box p={2}>
          {children}
        </Box>
      </Card>
  )
};

export default Direction;
