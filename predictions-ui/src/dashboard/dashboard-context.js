import React from 'react'
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { useLocation } from "react-router-dom";
import _ from 'lodash';

const DashboardContext = React.createContext({});

am4core.useTheme(am4themes_animated);
am4core.options.queue = true;
am4core.options.onlyShowOnViewport = true;


// This context provider is passed to any component requiring the context
const Provider = (props) => {
  const {children} = props;
  const [loading, setLoading] = React.useState(true);
  const [error, setError] = React.useState(false);
  const [predicted, setPredicted] = React.useState([]);
  const [consolidated, setConsolidated] = React.useState([]);
  const [target, setTarget] = React.useState([]);

  const location = useLocation();
  const model_path = location.pathname.replace("/", "");


  const getData = async (filePath) => {
    try {
      const response = await fetch(
        `https://****************.amazonaws.com/default/download-csv-dev-download-csv?location=${filePath}`
      );
      const data = await response.json();
      return data.input;
    } catch { 
      setError(true);
      return false;
    }
  };

  React.useEffect(() => {
    const loadData = async () => {
      setLoading(true);

      // Load if it's not loaded yet
      if (_.isEmpty(consolidated)) {
        const localConsolidated = await getData("consolidated/raw_consolidated.csv");
        if (localConsolidated) setConsolidated(localConsolidated);
      }
      if (_.isEmpty(target)) {
        const localTarget = await getData("consolidated/target.csv");
        if (localTarget) setTarget(localTarget);
      }

      let localPredicted;
      if (model_path == 'combined'){
        const softPredicted = await getData(`predictions/predicted_combined_soft.csv`);
        const hardPredicted = await getData(`predictions/predicted_combined_hard.csv`);
        localPredicted = { soft: softPredicted, hard: hardPredicted }
      } else {
        localPredicted = await getData(`predictions/predicted_${model_path}.csv`);
      }

      if (localPredicted) setPredicted(localPredicted);

      setLoading(false);
    };

    if (model_path) loadData();
  }, [model_path]);
  
  return (
    <DashboardContext.Provider
      value={{
        predicted,
        target,
        consolidated,
        error,
        loading,

        am4charts,
        am4core,
        ...props,
      }}
    >
      {children}
    </DashboardContext.Provider>
  );
};

const DashboardContextProvider = Provider;

export { DashboardContext };
export { DashboardContextProvider };
