
import React from 'react'
import {ReactComponent as LogoViu} from './logo-viu.svg';

import { Box, AppBar, Tabs, Tab } from '@material-ui/core';
import { makeStyles  } from '@material-ui/styles';

import { useLocation } from "react-router-dom";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  headerWrapper: {
    position: 'fixed',
    top: 0,
    width: '100%',
    zIndex: 1000,
  },
  header: {
    color: 'white',
    background: 'black',
    display: 'flex',
    alignItems: 'center',
    width: '100%',
  },
  navbar: {
    background: '#e8560f',
    width: '100%',
  },
  indicator: {
    background: 'black',
  }
}));

const DashboardHeader = (props) => {
  const classes = useStyles();
  const location = useLocation();
  const path = location.pathname;
  const currentTab = path.includes("combined") 
                      ? 0 : path.includes("first_model") 
                      ? 1 : path.includes("/second_model") 
                      ? 2 : 3;

  return (
    <Box className={classes.headerWrapper}>
      <Box height="110px" className={classes.header} px={4} display="flex">
        <Box flexGrow={1}>
          <LogoViu style={{height:'90px'}}/>
        </Box>
        <Box width="500px">
          <table align="left" style={{ width:"100%" }}>
            <tr>
              <th><span style={{color: '#f37b3f'}}>Nombre: </span></th>
              <th>Sergio Köller</th>
            </tr>
            <tr>
              <th><span style={{color: '#f37b3f'}}>Carrera: </span></th>
              <th>Master en Inteligencia Artificial</th>
            </tr>
            <tr>
              <th><span style={{color: '#f37b3f'}}>Director: </span></th>
              <th>María de los Ángeles Rodríguez Sánchez</th>
            </tr>
          </table>
        </Box>
      </Box>
      <Box>
        <AppBar position="static" className={classes.navbar}>
          <Tabs 
            value={currentTab} 
            aria-label="Modelos"
            variant="fullWidth"
            classes={{
              indicator: classes.indicator
            }}
          >
            <Tab to="/combined" component={Link} label="Meta Modelo" />
            <Tab to="/first_model" component={Link} label="CNN + VADER" />
            <Tab to="/second_model" component={Link} label="LSTM + VADER" />
            <Tab to="/third_model" component={Link} label="BID-LSTM + VADER" />
          </Tabs>
        </AppBar>
      </Box>
  </Box>
)};

export default DashboardHeader;