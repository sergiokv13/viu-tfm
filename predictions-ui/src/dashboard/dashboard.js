import React from 'react'
import Header from './dashboard-header';
import ModelCharts from './model-charts';
import CombinedCharts from './combined-charts';

import { Redirect, Switch, Route } from "react-router-dom";

const Dashboard = (props) => {
  return (
    <div>
      <Header />
      <Switch>
        <Route path={`/combined`}>
          <CombinedCharts title="Meta Modelo" />
        </Route>
        <Route path={`/first_model`}>
          <ModelCharts title="CNN + VADER"/>
        </Route>
        <Route path={`/second_model`}>
          <ModelCharts title="LSTM + VADER" />
        </Route>
        <Route path={`/third_model`}>
          <ModelCharts title="ANN + VADER" />
        </Route>
        <Route path="*">
          <Redirect to="/combined" />
        </Route> 
      </Switch>
    </div>
  )
};

export default Dashboard;
