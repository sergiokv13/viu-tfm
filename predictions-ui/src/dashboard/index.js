import React from 'react'
import Dashboard from './dashboard';
import { DashboardContextProvider } from './dashboard-context';

const DashboardIndex = (props) => {

  return (
    <DashboardContextProvider {...props}>
      <Dashboard
        {...props}
      />
    </DashboardContextProvider>
  )
}

export default DashboardIndex;
