import React from 'react'
import * as Charts from './charts';
import { DashboardContext } from './dashboard-context';
import CenteredLoader from './centered-loader';
import _ from 'lodash';

import { Grid, Box } from '@material-ui/core';

const ModelCharts = ({ model, title }) => {
  const context = React.useContext(DashboardContext);
  const { loading, predicted } = context;

  return (
    <Box padding={4} height="100vh" mt="150px">
      <Grid container spacing={4}>
        <Grid item xs={12}>
          <h1 style={{color: '#e8560f'}}>{title}</h1>
        </Grid>

        {
          loading
          ? <CenteredLoader top="100px"/>
          : (
            <>
              <Grid item xs={6}>
                <Charts.Prediction model={model} predicted={predicted?.soft || predicted} />
              </Grid>
              <Grid item xs={6}>
                <Charts.Accuracy model={model} predicted={predicted?.soft || predicted} />
              </Grid>
              <Grid item xs={12}>
                <Charts.Direction model={model} predicted={predicted?.soft || predicted} />
              </Grid>
            </>
          )
        }

      </Grid>
    </Box>
  )
};

export default ModelCharts;
