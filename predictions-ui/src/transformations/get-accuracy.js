// Get value > 0.5 -> 1 else 0
const gV = (val) => val > 0.5 ? 1 : 0;

const getAccuracy = (real, predicted, onlyOne) => {

  let totalCount = 0;
  let totalCountTrue = 0;

  let totalCountMid = 0;
  let totalCountMidTrue = 0;

  let totalCountHigh = 0;
  let totalCountHighTrue = 0;

  predicted.forEach((row) => {
    const date = row.date;
    const predClose = row.predicted;
    const realClose = real.find((realRow) => realRow.date == date)?.close_trend;;
    if (realClose !== undefined) {
      totalCount += 1;

      if (gV(predClose) == realClose) totalCountTrue += 1;

      if (predClose >= 0.6 || predClose <= 0.4) {
        totalCountMid += 1;
        if (gV(predClose) == realClose) totalCountMidTrue  += 1;
      }

      if (predClose >= 0.8 || predClose <= 0.2) {
        totalCountHigh += 1;
        if (gV(predClose) == realClose) totalCountHighTrue  += 1;
      }
    }    
  });


  return onlyOne ? [{
      "range": "Accuracy",
      "value":  (totalCountHighTrue / totalCountHigh),
    }] : [{
      "range": "Señal fuerte",
      "value":  (totalCountHighTrue / totalCountHigh),
    }, {
      "range": "Señal media",
      "value": (totalCountMidTrue / totalCountMid),
    }, {
      "range": "Total",
      "value": (totalCountTrue / totalCount),
    }];
};

export { getAccuracy };